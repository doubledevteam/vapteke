//
//  Colors.swift
//  Asko
//
//  Created by Ryan9Gray on 04.09.16.
//  Copyright © 2016 DoubleDevTeam. All rights reserved.
//

import Foundation

extension UIColor{
    class func customGreen() -> UIColor{
        return UIColor(red:0.41, green:0.74, blue:0.27, alpha:1.00)
    }
    class func customGrey() -> UIColor{
        //return UIColor(red:0.87, green:0.88, blue:0.89, alpha:1.00)
        return UIColor(red:0.89, green:0.91, blue:0.91, alpha:1.00)

    }
    class func customOrange() -> UIColor{
        return UIColor(red:0.76, green:0.39, blue:0.06, alpha:1.00)
        
    }
}
