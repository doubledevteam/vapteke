//
//  ErrorCenter.swift
//  GeekTV
//
//  Created by Ryan9Gray on 16.10.16.
//  Copyright © 2016 DoubleDevTeam. All rights reserved.
//

import Foundation
public struct ErrorCenter {
    
    public static func errorMsaga(error: String) {
        
        DispatchQueue.main.async(execute: {
            let HTTPAlert = UIAlertController(title: "Ошибка ", message: error, preferredStyle: .alert)
            
            HTTPAlert.addAction(UIAlertAction(title: "Ок", style: .default, handler: { (action: UIAlertAction!) in
                print("User clicked OK")
            }))
            UIApplication.shared.keyWindow?.rootViewController?.present(HTTPAlert, animated: true, completion: nil)
        })
    }
    public static func mesaga(error: String) {
        
        DispatchQueue.main.async(execute: {
            let HTTPAlert = UIAlertController(title: nil, message: error, preferredStyle: .alert)
            
            HTTPAlert.addAction(UIAlertAction(title: "Ок", style: .default, handler: { (action: UIAlertAction!) in
                print("User clicked OK")
            }))
            UIApplication.shared.keyWindow?.rootViewController?.present(HTTPAlert, animated: true, completion: nil)
        })
    }
    public static func handleResponse(_ error: Error?) {
        DispatchQueue.main.async(execute: {
            let HTTPAlert = UIAlertController(title: "Ошибка", message: "\(error!.localizedDescription)", preferredStyle: .alert)
            print("\(error!.localizedDescription)")
            HTTPAlert.addAction(UIAlertAction(title: "Ок", style: .default, handler: { (action: UIAlertAction!) in
                print("User clicked OK")
            }))
            UIApplication.shared.keyWindow?.rootViewController?.present(HTTPAlert, animated: true, completion: nil)
        })
    }

}
