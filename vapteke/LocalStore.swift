//
//  LocalStore.swift
//  Asko
//
//  Created by Ryan9Gray on 11.08.16.
//  Copyright © 2016 DoubleDevTeam. All rights reserved.
//

import UIKit


struct LocalStore {

    fileprivate static let firstLaunchKey = "firstLaunchKey"
    fileprivate static let cityIDKey = "City_ID"


    fileprivate static let userDefaults = UserDefaults.standard

    
//    static func setCity(_ token: String) {
//        userDefaults.set(token, forKey: cityIDKey)
//        userDefaults.synchronize()
//    }
//    static func getCity() -> String? {
//        return userDefaults.string(forKey: cityIDKey)
//    }

    static func setCity(_ name: String?, idNum: String?) {
        var city : [String: Any] = [:]
        if let fName = idNum{
            city["id"] = fName
        }
        if let sName = name{
            city["name"] = sName
        }
        
        userDefaults.set(city, forKey: cityIDKey)
        userDefaults.synchronize()
    }
    static func getCity() -> [String:Any]? {
        if let city = UserDefaults.standard.dictionary(forKey: cityIDKey) {
            print(city)
            return city
        }
        return nil
    }

    static func firstLaunch() -> Bool {
        if userDefaults.bool(forKey: firstLaunchKey){
            return false
        }
        else{
            userDefaults.set(true, forKey: firstLaunchKey)
            userDefaults.synchronize()
            return true
        }
    }

    
        
}
