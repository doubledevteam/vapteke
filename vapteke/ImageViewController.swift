//
//  ImageViewController.swift
//  vapteke
//
//  Created by Ryan9Gray on 14.12.2017.
//  Copyright © 2017 apteka. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {
    @IBOutlet var image: UIImageView!
    
    var tovarImage : UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.image.image = tovarImage
        // Do any additional setup after loading the view.
    }
    @IBAction func imageTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
