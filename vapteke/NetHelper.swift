
//
//  NetHelper.swift
//  Asko
//
//  Created by Ryan9Gray on 18.08.16.
//  Copyright © 2016 DoubleDevTeam. All rights reserved.
//

import Alamofire
import SwiftyJSON
import CoreLocation
import AlamofireImage

class  NetHelper {

    static let sharedInstance = NetHelper()
    private init(){}
    
    fileprivate let domen = "https://vapteke.ru/api/"
    fileprivate let udid = UIDevice.current.identifierForVendor!.uuidString
    fileprivate let headers: HTTPHeaders = [
        "api-key":"55c599b6fcbb629a02a03b261954d239","device":"ios","id":UIDevice.current.identifierForVendor!.uuidString]
    
    func coordParse(dic:JSON) -> CLLocationCoordinate2D{
        let lat = dic["coord"]["lat"].doubleValue
        let lng = dic["coord"]["lng"].doubleValue
        return CLLocationCoordinate2D(latitude: lat, longitude: lng)
    }
    
    
    func getCitys(closer: @escaping (_ arr: [City]?) -> ()) {
        
        let param = ["country_id":"1"]
        
        Alamofire.request(domen + "v1/cities" ,method: .post, parameters : param, headers: self.headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        if let info = json["data"].array{
                            //print("DataRows \(info)")
                            var officeArr = [City]()
                            for dic in info {
                                let phone = dic["phone"].string ?? ""
                                let name = dic["title"].string ?? ""
                                let cityID = dic["city_id"].string ?? ""
                                let cit = City(cityID: cityID, phone: phone, name: name)
                                officeArr.append(cit)
                            }
                            closer(officeArr)
                            return
                        }
                    }
                case .failure(let error):
                    print(error)
                    closer(nil)
                    ErrorCenter.handleResponse(error)
                }
        }
    }
    func getNearsApps(closer: @escaping (_ arr: [AptekaAddress]?) -> ()) {

        let urlString = domen + "v1/drugStores"
        
        guard let city = LocalStore.getCity()!["id"] else{
            return
        }
        let param = ["city_id":"\(city)"]
        
        Alamofire.request(urlString, method: .post, parameters: param, headers: self.headers )
            .validate().responseJSON { response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        //print(json)
                        if let info = json["data"].array{
                            var officeArr = [AptekaAddress]()
                            for dic in info {
                                let name = dic["title"].string ?? ""
                                let kodApt = dic["id"].string ?? ""
                                let adres = dic["address"].string ?? ""
                                let time = dic["schedule"].string ?? ""
                                let phone = dic["phone"].string ?? ""
                                let coordinate = self.coordParse(dic: dic)
                                let hours = dic["roundClock"].bool ?? false
                                let hour24 : Int = hours ? 1 : 0
                      
                                let apt = AptekaAddress(title: name, locationName: adres, telephone: phone, coordinate: coordinate, kod: kodApt, timing:time, hour24: hour24)
                                officeArr.append(apt)
                            }
                            closer(officeArr)
                        }
                    }
                    print("Validation Successful")
                case .failure(let error):
                    print(error)
                    closer(nil)
                }
        }
    }
    
    func searchHint(_ name: String, closer: @escaping (_ arr: [String]?) -> ()) {
        if name == ""{
            return
        }
        let urlString = domen + "v1/searchTips"

        let param = ["query":name]

        Alamofire.request(urlString,method: .post, parameters : param, headers: self.headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        if let info = json["data"].array{
                            print("info \(info)")
                            var officeArr = [String]()
                            for dic in info {
                                let value = dic["name"].string ?? ""
                                officeArr.append(value)
                            }
                            closer(officeArr)
                            return
                        }
                    }
                case .failure(let error):
                    print(error)
                    closer(nil)
                    //handleResponse(error)
                }
        }
    }
    
    func searchTovar(_ name: String, kodApteka:String?, kodDistrict:String? = nil, closer: @escaping (_ arr: [Cure]?) -> ()) {
        
        guard let city = LocalStore.getCity()!["id"] else{
            return
        }
        let urlString = domen + "v1/search"
        var param = ["query":name, "city_id":"\(city)"]
        
        if let apt = kodApteka {
            param["apt_id"] = apt
        }
        if let dis = kodDistrict {
            param["district_id"] = dis
        }
        Alamofire.request(urlString, method: .post, parameters : param, headers: self.headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        
                        if let info = json["data"].array{
                            print("info \(info)")
                            var officeArr = [Cure]()
                            for dic in info {
                                let tov = Cure()
                                tov.title = dic["title"].string ?? ""
                                tov.form = dic["form"].string ?? ""
                                tov.tovKod = dic["id"].string ?? ""
                                tov.imgKod = dic["image"].string ?? ""
                                tov.priceMin = dic["priceMin"].double ?? 0.00
                                
                                if let man = dic["manufacturer"].dictionary {
                                    tov.manufacturer = FormFaktor.init(title: man["title"]?.string ?? "", kod: man["id"]?.string ?? "")
                                }
                                let syn = dic["isSynonym"].string ?? ""
                                if syn == "1"{
                                    tov.isSynonym = true
                                }
                                officeArr.append(tov)
                            }
                            closer(officeArr)
                            return
                        }
                        closer(nil)

                    }
                case .failure(let error):
                    print(error)
                    closer(nil)
                    ErrorCenter.handleResponse(error)
                }
        }
    }

    func tovarItem(_ kodItem: String, kodManuf:String?, closer: @escaping (_ arr: Tovar?) -> ()) {
        
        guard let city = LocalStore.getCity()!["id"] else{
            return
        }
        let urlString = domen + "v1/item"
        var param = ["item_id":kodItem, "city_id":"\(city)"]
        
        if let man = kodManuf {
            param["manufacturer_id"] = man
        }

        Alamofire.request(urlString, method: .post, parameters : param, headers: self.headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        
                        if let dic = json["data"].dictionary {
                            print("info \(dic)")
                            let tov = Tovar()
                            tov.title = dic["title"]?.string ?? ""
                            tov.form = dic["form"]?.string ?? ""
                            tov.mnn = dic["mnn"]?.string ?? ""
                            tov.tovKod = dic["id"]?.string ?? ""
                            tov.imgKod = dic["image"]?.string ?? ""
                            
                            if let man = dic["manufacturer"]?.dictionary {
                                tov.manufacturer = FormFaktor.init(title: man["title"]?.string ?? "", kod: man["id"]?.string ?? "")
                            }
                            for man in dic["manufacturers"]?.array ?? []{
                                tov.manufacturers.append(FormFaktor(title: man["title"].string ?? "", kod: man["id"].string ?? ""))
                            }
                            for form in dic["item_forms"]?.array ?? []{
                                tov.forms.append(FormFaktor(title: form["name"].string ?? "", kod: form["item_id"].string ?? ""))
                            }
                            
                            let priceMax = dic["priceMax"]?.double ?? 0.00
                            let priceMin = dic["priceMin"]?.double ?? 0.00
                            let priceOrderMin = dic["priceOrderMin"]?.double ?? 0.00
                            let priceOrderMax = dic["priceOrderMax"]?.double ?? 0.00
                            tov.price = Price(max: priceMax, min: priceMin)
                            tov.priceOrder = Price(max: priceOrderMax, min: priceOrderMin)

                            closer(tov)
                            return
                        }
                        closer(nil)
                        
                    }
                case .failure(let error):
                    print(error)
                    closer(nil)
                    ErrorCenter.handleResponse(error)
                }
        }
    }
    
    func getItemPrices(_ kodItem: String, kodManuf:String?, apteka:String?, closer: @escaping (_ arr: [ItemPrice]) -> ()) {
        
        guard let city = LocalStore.getCity()!["id"] else{
            return
        }
        let urlString = domen + "v1/itemPrices"
        var param = ["item_id":kodItem, "city_id":"\(city)"]
        if let man = kodManuf {
            param["manufacturer_id"] = man
        }
        if let apt = apteka {
            param["apt_id"] = apt
        }
        Alamofire.request(urlString, method: .post, parameters : param, headers: self.headers)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        
                        if let dic = json["data"].dictionary {
                            print("info \(dic)")
                            var items : [ItemPrice] = []
                            
                            closer(items)
                            return
                        }
                        closer([])
                        
                    }
                case .failure(let error):
                    print(error)
                    closer([])
                    ErrorCenter.handleResponse(error)
                }
        }
    }
    
    
    func imgLoad(_ url:String, closer: @escaping (_ img: UIImage?) -> ()){
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                print("image downloaded: \(image)")
                closer(image)
            }
        }
    }

    
    func debugREsponse(response: DataResponse<Any>) {
        let request = response.request
        print("Request: \(request!.httpMethod!) - \(request!.url!.absoluteString)\n\(request!.httpBody.map { body in String(data: body, encoding: .utf8) ?? "" } ?? "")")
        print("\(String(describing: response.request))")
        print("\(String(describing: response.response))")
        print("\(String(describing: response.data))")
        print("\(String(describing: response.result.value))")
    }
    
    
}
