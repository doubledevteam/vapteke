//
//  UIViewExt.swift
//  GeekTV
//
//  Created by Ryan9Gray on 27.09.16.
//  Copyright © 2016 DoubleDevTeam. All rights reserved.
//

import Foundation
extension UIViewController {
    
    func setSlider(){
        if revealViewController() != nil {
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }

    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func keyboardWillShow(_ notification: Notification) {
        
        if let keyboardSize = ((notification as NSNotification).userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height-20
            }
            else {
                
            }
        }
        
    }
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = ((notification as NSNotification).userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height-20
            }
            else {
                
            }
        }
    }
    func handleResponse(error: NSError?) {
        
        DispatchQueue.main.async(execute: {
            let HTTPAlert = UIAlertController(title: "Ошибка \(error!.code)", message: "\(error!.localizedDescription)", preferredStyle: .alert)
            
            print("\(error!.localizedDescription)")
            HTTPAlert.addAction(UIAlertAction(title: "Ок", style: .default, handler: { (action: UIAlertAction!) in
                print("User clicked OK")
            }))
            UIApplication.shared.keyWindow?.rootViewController?.present(HTTPAlert, animated: true, completion: nil)
        })
    }
    func errorMesaga(error: String) {
        
        DispatchQueue.main.async(execute: {
            let HTTPAlert = UIAlertController(title: "Ошибка ", message: error, preferredStyle: .alert)
            
            HTTPAlert.addAction(UIAlertAction(title: "Ок", style: .default, handler: { (action: UIAlertAction!) in
                print("User clicked OK")
            }))
            UIApplication.shared.keyWindow?.rootViewController?.present(HTTPAlert, animated: true, completion: nil)
        })
    }


    func goTo(_ viewSeag:String) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: viewSeag)
        let rvc:SWRevealViewController = self.revealViewController() as SWRevealViewController
        rvc.pushFrontViewController(vc, animated: true)
    }
    
    func goToMain() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MainViewControllerStoryboardId")
        let rvc:SWRevealViewController = self.revealViewController() as SWRevealViewController
        rvc.pushFrontViewController(vc, animated: true)
    }
    
}

extension UIView{
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
extension Dictionary {
    mutating func update(other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
}
extension Array where Element: Hashable {
    var setValue: Array<Element> {
        let s = Set<Element>(self)
        return Array(s)
    }
}
extension NSMutableAttributedString {
    @discardableResult func bold(_ text:String) -> NSMutableAttributedString {
        let attrs:[NSAttributedStringKey:Any] = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 16)]
        let boldString = NSMutableAttributedString(string: text, attributes:attrs)
        self.append(boldString)
        return self
    }
    
    @discardableResult func normal(_ text:String)->NSMutableAttributedString {
        let normal =  NSAttributedString(string: text)
        self.append(normal)
        return self
    }
}


extension String
{
    func replace(target: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
    }

    var utf8Data: Data? {
        return data(using: String.Encoding.utf8)
    }
    
    var price : String {
        if let d = Double(self){
            return String(format:"%.2f₽", d)
        }
        else{
            return "--"//self
        }
    }
    
}
extension Data {
    var attributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options:[NSAttributedString.DocumentReadingOptionKey.documentType :NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return nil
    }
}
extension Date {
    var month: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM"
        return dateFormatter.string(from: self)
    }
    var hour0x: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh"
        return dateFormatter.string(from: self)
    }
    var minute0x: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "mm"
        return dateFormatter.string(from: self)
    }
    
}

extension UIView {
    
    class func loadFromNibNamed(nibNamed: String, bundle : Bundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
}

extension UIApplication {
    class func openAppSettings() {
        if let url = URL(string:UIApplicationOpenSettingsURLString) {
            UIApplication.shared.openURL(url)
        }
    }
}
extension UIImage {
    
    func imageResize (sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        self.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    
}
