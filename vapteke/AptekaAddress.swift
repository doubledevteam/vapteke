//
//  OfficeAddress.swift
//  Asko
//
//  Created by Ryan9Gray on 15.08.16.
//  Copyright © 2016 DoubleDevTeam. All rights reserved.
//

import Foundation
import MapKit

struct ItemPrice {
    let id : String
    let price : String
    let priceOrder : String

}

final class AptekaAddress: NSObject, MKAnnotation {
    let title: String?
    let kod : String
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    let telephone: String
    let timing: String
    let hour24: Int

    
    init(title: String, locationName: String, telephone: String, coordinate: CLLocationCoordinate2D, kod:String,timing:String, hour24:Int) {
        self.title = title
        self.locationName = locationName
        self.telephone = telephone
        self.coordinate = coordinate
        self.kod = kod
        self.timing = timing
        self.hour24 = hour24
        super.init()
        }
    
    
    var subtitle: String? {
        return locationName
    }

    
    // annotation callout opens this mapItem in Maps app
    func mapItem() -> MKMapItem {
        //let addressDict = self.subtitle
        let placemark = MKPlacemark(coordinate: self.coordinate, addressDictionary: nil)
        
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = self.title
        
        return mapItem
    }
    
    
}
