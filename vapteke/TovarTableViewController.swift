//
//  TovarTableViewController.swift
//  vapteke
//
//  Created by Ryan9Gray on 10.12.2017.
//  Copyright © 2017 apteka. All rights reserved.
//

import UIKit

protocol MasterTovarDelegate: class {
    
    func controllerDidTovar(data: String, controller: TovarTableViewController)
}

class TovarTableViewController: UITableViewController, HandleSearchResult, UISearchBarDelegate, photoClickDelegate  {
  
    var parentNavigationController : UINavigationController?
    var customSearchController: CustomSearchController!
    
    weak var masterDelegate : MasterTovarDelegate!

    var tovars = [Cure](){
        didSet{
            if tovars.count > 0{
                self.reloadData()
            }
        }
    }

    var apteka : AptekaAddress! {
        didSet{
            self.searchStart(word: self.customSearchController.searchBar.text!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.configureCustomSearchController()
        NotificationCenter.default.addObserver(self, selector: #selector(handleApteka(notification:)), name:NSNotification.Name(rawValue: "APTEKASEARCH"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.title = "Поиск товара"
        
    }
    func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    func openImageTag(_ imageTag: String) {
        NetHelper.sharedInstance.imgLoad(imageTag) { (img) in
            if let image = img{
                self.openImage(image)
            }
        }
    }
    func openImage(_ image:UIImage){

        performSegue(withIdentifier: "openImage", sender: image)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "openImage" {
            let toView = segue.destination as! ImageViewController
            if let img = sender as? UIImage {
                toView.tovarImage = img
            }
        }
    }
    @objc func handleApteka(notification:NSNotification){
        if let ap = notification.object as? AptekaAddress{
            self.apteka = ap
        }
    }
    func searchStart(word:String){
        self.view.showLoading()
        NetHelper.sharedInstance.searchTovar(word,kodApteka: self.apteka?.kod) { (t) in
            self.view.hideLoading()
            if let tov = t {
                self.tovars = tov
            }
        }
    }
    
    func openCard(tovar:Tovar){
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ItemViewController") as! ItemViewController
        vc.tovar = tovar
        self.present(vc, animated: true, completion: nil)
    }
    
    func searchCard(tovar:Cure){
        self.view.showLoading()
        NetHelper.sharedInstance.tovarItem(tovar.tovKod, kodManuf: tovar.manufacturer?.kod) {  tovars in
            self.view.hideLoading()
            
            if let tovars = tovars {
                self.openCard(tovar: tovars)
            }
        }
    }
    
    func searchResultName(_ name:String){
        searchStart(word: name)
        self.customSearchController.searchBar.text = name
    }

    deinit{
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "APTEKASEARCH"), object:nil)
        self.tableView.dataSource = nil
        self.tableView.delegate = nil
        print("deinit feed")
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TovarCell", for: indexPath) as! TovarTableViewCell
        cell.photoDelegate = self
        cell.setTovar(tovar: self.tovars[indexPath.row])
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.searchCard(tovar: self.tovars[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func configureCustomSearchController() {
        
        let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "SearchResultTable") as! SearchResultsController
        locationSearchTable.handleSearchDelegate = self
        customSearchController = CustomSearchController(searchResultsController: locationSearchTable, searchBarFrame:  CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: 56.0))
        
        customSearchController.searchBar.placeholder = "ПОИСК ТОВАРА"
        customSearchController.searchResultsUpdater = locationSearchTable
        self.tableView.tableHeaderView = customSearchController.searchBar
        customSearchController.searchBar.sizeToFit()
        self.view.layoutIfNeeded()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tovars.count
    }
}
