//
//  LocationService.swift
//
//
//  Created by Anak Mirasing on 5/18/2558 BE.
//
//

import Foundation
import CoreLocation

protocol LocationServiceDelegate {
    func tracingLocation(_ currentLocation: CLLocation)
    func tracingCity(_ city: String)
    func tracingLocationDidFailWithError(_ error: Error)
}

extension LocationServiceDelegate{
    
    func tracingLocationDidFailWithError(_ error: Error) {
        print("tracing Location Error : \(error.localizedDescription)")
        let HTTPAlert = UIAlertController(title: "Ошибка местоположения", message: "\(error.localizedDescription)", preferredStyle: .alert)
        
        print("\(error.localizedDescription)")
        HTTPAlert.addAction(UIAlertAction(title: "Ок", style: .default, handler: { (action: UIAlertAction!) in
            print("User clicked OK")
        }))
        UIApplication.shared.keyWindow?.rootViewController?.present(HTTPAlert, animated: true, completion: nil)
    }
}

class LocationService: NSObject, CLLocationManagerDelegate {
    
    struct Static
    {
        fileprivate static var instance: LocationService?
    }
    
    class var sharedInstance: LocationService
    {
        if Static.instance == nil
        {
            Static.instance = LocationService()
        }
        
        return Static.instance!
    }
//    class var sharedInstance: LocationService {
//         struct Static {
//            static var onceToken: dispatch_once_t = 0
//            
//            static var instance: LocationService? = nil
//        }
//        dispatch_once(&Static.onceToken) {
//            Static.instance = LocationService()
//        }
//        return Static.instance!
//    }

    var locationManager: CLLocationManager?
    var lastLocation: CLLocation?
    var delegate: LocationServiceDelegate?
    var didFindLocation : Bool = true


    override init() {
        super.init()
        self.locationManager = CLLocationManager()
        guard let locationManager = self.locationManager else {
            return
        }
        
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            // ...
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            
            break
        case  .restricted, .denied:
            let alertController = UIAlertController(
                title: "Для продолжения работы необходимо определить Ваше местоположение",
                message: "Чтобы включить эту функцию, пройдите в Настройки и разрешите 'Геолокацию'",
                preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Отказать", style: .cancel) { (action) in
                
            NotificationCenter.default.post(name: Notification.Name(rawValue: "OFFLOCATION"), object:nil)
 
             self.destroy()
            }
            alertController.addAction(cancelAction)
            let openAction = UIAlertAction(title: "Настройки", style: .default) { (action) in
                if let url = URL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(url)
                }
            }
            alertController.addAction(openAction)
            
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
            break
        default:
            break
        }

        locationManager.desiredAccuracy = kCLLocationAccuracyBest // The accuracy of the location data
        locationManager.distanceFilter = 200 // The minimum distance (measured in meters) a device must move horizontally before an update event is generated.
        locationManager.delegate = self
    }
    
    func managerStatus(){
        
    }

    
    func startUpdatingLocation() {
        didFindLocation = true
        print("Starting Location Updates")
        self.locationManager?.startUpdatingLocation()
    }
    
    func stopUpdatingLocation() {
        print("Stop Location Updates")
        self.locationManager?.stopUpdatingLocation()
    }
    
    // CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        guard let location = locations.last else {
            return
        }
        self.locationManager?.stopUpdatingLocation()

        if didFindLocation==false {
            return
        }
        didFindLocation=false
        // singleton for get last location
        self.lastLocation = location
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error)->Void in
            if (error != nil)
            {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }

            if placemarks!.count > 0
            {
                let pm = placemarks![0] as CLPlacemark
                self.updateCity(pm)
            }
            else{
                print("Problem with the data received from geocoder")
            }
        })
        // use for real time update location
        updateLocation(location)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            self.locationManager?.requestLocation()
        }
        if status == .denied{
            destroy()
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        // do on error
        updateLocationDidFailWithError(error)
    }
    
    // Private function
    fileprivate func updateLocation(_ currentLocation: CLLocation){

        guard let delegate = self.delegate else {
            return
        }
        
        delegate.tracingLocation(currentLocation)
    }
    
    fileprivate func updateCity(_ placemark: CLPlacemark?){

        guard let delegate = self.delegate else {
            return
        }
        if let containsPlacemark = placemark
        {
        let administrativeArea = (containsPlacemark.administrativeArea != nil) ? containsPlacemark.administrativeArea : ""
        if let city = administrativeArea{
            delegate.tracingCity(city)
        }
        print("City - \(String(describing: administrativeArea))")
        }
    }
    
    fileprivate func destroy(){
       // locationManager = nil
       LocationService.Static.instance = nil
    }
    
    
    fileprivate func updateLocationDidFailWithError(_ error: Error) {
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.tracingLocationDidFailWithError(error)
        destroy()
    }
}
