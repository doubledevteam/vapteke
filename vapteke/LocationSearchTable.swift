//
//  LocationSearchTable.swift
//  MapKitTutorial
//
//  Created by Robert Chen on 12/28/15.
//  Copyright © 2015 Thorn Technologies. All rights reserved.
//

import UIKit
import MapKit
protocol HandleSearchApteka : class {
    func searchResultApteka(_ apteka:AptekaAddress)
}
class LocationSearchTable : UITableViewController, UISearchBarDelegate {
    var originalData : [AptekaAddress] = []
    var filteredData : [AptekaAddress] = []
    var word = ""
    weak var handleSearchDelegate:HandleSearchApteka? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"aptekaCell", for: indexPath) as! AptekaTableViewCell
        cell.setUpCell(apteka: filteredData[indexPath.row])
        return cell
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // to limit network activity, reload half a second after last key press.
        if searchText.isEmpty {
            
        }
    }

}
    
extension LocationSearchTable {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = filteredData[indexPath.row]
        handleSearchDelegate?.searchResultApteka(selectedItem)
        dismiss(animated: true, completion: nil)
    }
}
extension LocationSearchTable : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let sb = searchController.searchBar
        let target = sb.text!
        self.filteredData = originalData.filter({ (apteka) -> Bool in
            return apteka.title!.lowercased().contains(target.lowercased())
        })
        self.tableView.reloadData()
    }
}


