//
//  ViewController.swift
//  vapteke
//
//  Created by Ryan9Gray on 09.09.17.
//  Copyright © 2017 apteka. All rights reserved.
//

import UIKit

class MasterSearchViewController: UIViewController, CAPSPageMenuDelegate, MasterMapDelegate, MasterTovarDelegate  {

    private var pageMenu : CAPSPageMenu?
    
    @IBOutlet var menuButton: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if revealViewController() != nil {
            self.menuButton.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for:UIControlEvents.touchUpInside)
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.menuButton.widthAnchor.constraint(equalToConstant: 28.0).isActive = true
        self.menuButton.heightAnchor.constraint(equalToConstant: 18.0).isActive = true

        NotificationCenter.default.addObserver(self, selector: #selector(handleApteka(notification:)), name:NSNotification.Name(rawValue: "APTEKASEARCH"), object: nil)
        setUp()
        self.title = "Найти товар"
    }
    @objc func handleApteka(notification:NSNotification){
     self.pageMenu?.moveToPage(0)
    }

    func setUp(){
        //self.title = "Кабинет"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.tintColor = UIColor.black
        
        // MARK: - Scroll menu setup
        
        // Initialize view controllers to display and place in array
        var controllerArray : [UIViewController] = []
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        //let controller2 : SearchTovarViewController = storyboard.instantiateViewController(withIdentifier: "tovarSearchID") as! SearchTovarViewController
        
        let controller2 : TovarTableViewController = storyboard.instantiateViewController(withIdentifier: "TovarTable") as! TovarTableViewController
        controller2.title = "ПОИСК ТОВАРОВ"
        controller2.parentNavigationController = self.navigationController
        controller2.masterDelegate = self
        controllerArray.append(controller2)
        
        let controller1 : SearchAptekaViewController = storyboard.instantiateViewController(withIdentifier: "aptekaSearchID") as! SearchAptekaViewController
        controller1.parentNavigationController = self.navigationController
        controller1.title = "АПТЕКИ"
        controller1.masterDelegate = self
        controllerArray.append(controller1)
        
        let parameters: [CAPSPageMenuOption] = [
            //.menuItemSeparatorWidth(4.3),
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 247.0/255.0, alpha: 1.0)),
            .bottomMenuHairlineColor(UIColor(red: 20.0/255.0, green: 20.0/255.0, blue: 20.0/255.0, alpha: 0.1)),
            .selectionIndicatorColor(UIColor.customGreen()),
            .menuMargin(74.0),
            .menuHeight(34.0),
            //.menuItemWidth(140.0),
            .selectedMenuItemLabelColor(UIColor.black),
            .unselectedMenuItemLabelColor(UIColor(red: 40.0/255.0, green: 40.0/255.0, blue: 40.0/255.0, alpha: 1.0)),
            .menuItemFont(UIFont(name: "HelveticaNeue-Medium", size: 16.0)!),
            .useMenuLikeSegmentedControl(false),
            .menuItemSeparatorRoundEdges(false),
            .selectionIndicatorHeight(2.0),
            .centerMenuItems(true),
            //.hideTopMenuBar(false),
            .menuItemWidthBasedOnTitleTextWidth(true),
            .enableHorizontalBounce(true)
            //.menuItemSeparatorPercentageHeight(0.1)
        ]
        
        // Initialize scroll menu
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        // Optional delegate
        pageMenu!.delegate = self
        

        self.view.addSubview(pageMenu!.view)
        //self.view.sendSubview(toBack: pageMenu!.view)
        self.addChildViewController(pageMenu!)
        //pageMenu!.didMove(toParentViewController: self)
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        print("did move to page")
    }
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        print("will move to page")
        self.title = index == 0 ? "Найти товар" : "В аптеке"
    }
    func controllerDidTovar(data: String, controller: TovarTableViewController) {
        
    }
    
    func controllerDidApteka(data: String, controller: SearchAptekaViewController) {
        
    }
    

    deinit{
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "APTEKASEARCH"), object:nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

