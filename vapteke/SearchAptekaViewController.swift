//
//  SearchAptekaViewController.swift
//  vapteke
//
//  Created by Ryan9Gray on 16.09.17.
//  Copyright © 2017 apteka. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Spring

protocol HandleMapSearch {
    func dropPinZoomIn(_ placemark:MKPlacemark)
}
protocol MasterMapDelegate: class {
    
    func controllerDidApteka(data: String, controller: SearchAptekaViewController)
}

class SearchAptekaViewController: UIViewController , HandleSearchApteka, UISearchBarDelegate, SearchSettingsDelegate   {

    var parentNavigationController : UINavigationController?
    let bottomSheetVC = BottomSheetViewController()
    var mapView: MKMapView!
    var coordinate : [String : Double]!
    var customSearchController: CustomSearchController!
    var locationSearchTable: LocationSearchTable!
    
    weak var masterDelegate : MasterMapDelegate!
    
    @IBOutlet var settingsSearchView : UIView!

    
    //MARK:VAR
    var apteks : [AptekaAddress] = []{
        didSet{
            loadMaps(self.apteks)
            locationSearchTable.originalData = self.apteks
        }
    }
    var apteka : AptekaAddress! {
        didSet{
            setApteka(aptek: self.apteka)
        }
    }
    
    func hour24Show(on:Bool){
        if on{
            let h24 = self.apteks.filter{$0.hour24 == 1}
            loadMaps(h24)
        }
        else{
            loadMaps(self.apteks)
        }
    }
    
    func setApteka(aptek:AptekaAddress){
        //self.bottomDelegate?.setApteka(aptek)
        self.bottomSheetVC.apteka = aptek
    }

    @IBAction func h24Click(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        sender.backgroundColor = sender.isSelected ? UIColor.customGreen() : UIColor.customGrey()
        hour24Show(on: sender.isSelected)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()

        self.createMap()
        LocationService.sharedInstance.delegate = self

        searchSet()
        addBottomSheetView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setPinsOfCity()
        
    }
    
    func addBottomSheetView() {
        self.addChildViewController(self.bottomSheetVC)
        self.view.addSubview(self.bottomSheetVC.view)
        self.bottomSheetVC.didMove(toParentViewController: self)
        let height = view.frame.height
        let width  = view.frame.width
        self.bottomSheetVC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
    func createMap(){
    
        let leftMargin:CGFloat = 0
        let topMargin:CGFloat = 60
        let mapWidth:CGFloat = view.frame.size.width
        let mapHeight:CGFloat = view.frame.size.height - 60
        self.mapView = MKMapView(frame:CGRect(x: leftMargin, y: topMargin, width: mapWidth, height: mapHeight))

       // self.mapView.frame = CGRect(x: leftMargin, y: topMargin, width: mapWidth, height: mapHeight)
        
        self.mapView.mapType = MKMapType.standard
        self.mapView.isZoomEnabled = true
        self.mapView.isScrollEnabled = true
        self.mapView.delegate = self
        
        // Or, if needed, we can position map in the center of the view
        self.mapView.center = view.center
        self.mapView.showsUserLocation = true

        view.addSubview(self.mapView)
        //self.view.bringSubview(toFront: self.apInfoView.superview!)
    }
    
    
    func setPinsOfCity(){
        self.view.showLoading()
        NetHelper.sharedInstance.getNearsApps() { app in
            if let ap = app{
                self.apteks = ap
            }
            self.view.hideLoading()
        }
    }

    
    func searchSet(){
        locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "LocationSearchTable") as! LocationSearchTable
        locationSearchTable.handleSearchDelegate = self
        locationSearchTable.originalData = self.apteks
        customSearchController = CustomSearchController(searchResultsController: locationSearchTable, searchBarFrame:  CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: 56.0))
        customSearchController.searchBar.placeholder = "ПОИСК АПТЕКИ"
        customSearchController.searchResultsUpdater = locationSearchTable
        //customSearchController.searchBar.delegate = self
        // customSearchController.customSearchBar.delegate = locationSearchTable

        customSearchController.settingsDelegate = self

        view.addSubview(customSearchController.searchBar)
        createSettingsSearch()
    }
    
    func createSettingsSearch(){

        self.settingsSearchView.isHidden = true
        self.view.insertSubview(self.settingsSearchView, belowSubview: self.customSearchController.searchBar)
    }
    
    
    func settingsClick(_ on: Bool) {
        UIView.animate(withDuration: 0.5, animations: {
            self.settingsSearchView.frame.origin.y += on ? -50 : 50
            if !on{
                self.settingsSearchView.isHidden = false
            }
        }, completion: { (finish) in
            if on{
                self.settingsSearchView.isHidden = true
            }
        })
    }
    
    func searchResultApteka(_ apteka:AptekaAddress){
        customSearchController.searchBar.text = ""
        self.apteka = apteka
        selectAptekaPin(apteka)
    }
    
    func selectAptekaPin(_ apteka:AptekaAddress){
        
        self.apteka = apteka
        mapView.selectAnnotation(apteka, animated: true)
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(apteka.coordinate, span)
        mapView.setRegion(region, animated: true)
    }
    
    func loadMaps(_ address : [AptekaAddress]){
        
        self.mapView.removeAnnotations(self.mapView.annotations)
        for shop in address  {
            self.mapView.addAnnotation(shop)
        }
        self.mapView.showAnnotations(address, animated: true)
        self.view.hideLoading()
    }
    
    //MARK: Services
    
    private func errorPins(){
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "Ошибка", message: "Немозможно отобразить. Проблемы с интернетом или определением местоположения.", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
//            HTTPAlert.addAction(UIAlertAction(title: "Выбрать город", style: .default, handler: { (action: UIAlertAction!) in
//                // self.regionListOpen(nil)
//            }))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func applyMapViewMemoryFix(){
        switch (self.mapView.mapType) {
        case MKMapType.hybrid:
            self.mapView.mapType = MKMapType.standard
            break;
        case MKMapType.standard:
            self.mapView.mapType = MKMapType.hybrid
            break;
        default:
            break;
        }
        self.mapView.showsUserLocation = false
        self.mapView.delegate = nil
        self.mapView.removeFromSuperview()
        self.mapView = nil
    }
    
    deinit {
        applyMapViewMemoryFix()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension SearchAptekaViewController:  MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if let annotation = view.annotation as? AptekaAddress {
            print("Your annotation title: \(annotation.title!)");
            self.selectAptekaPin(annotation)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "MyPin"
        
        if annotation.isKind(of: MKUserLocation.self) {
            return nil
        }
        
        var annotationView: MKPinAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
        }
        //let detailButton: UIButton = UIButton(type: UIButtonType.DetailDisclosure)
        //        let lefIconView = UIImageView(frame: CGRect(x: 0, y: 0, width: 53, height: 53))
        //        lefIconView.image = UIImage(named: "aColor")
        //        annotationView?.leftCalloutAccessoryView = lefIconView
        annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure) as UIView
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        let location = view.annotation as! AptekaAddress
        //goToRead(location)
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
}

extension SearchAptekaViewController:  LocationServiceDelegate {
    func tracingLocation(_ currentLocation: CLLocation) {
        let lat = currentLocation.coordinate.latitude
        let lon = currentLocation.coordinate.longitude
        
        self.coordinate = ["latitude":lat,"longitude":lon]
        
        //let center = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        //let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        //self.mapView.setRegion(region, animated: true)
    }
    func tracingCity(_ city: String) {
        
    }
}
