//
//  SearchTovarTableViewController.swift
//  vapteke
//
//  Created by Ryan9Gray on 16.09.17.
//  Copyright © 2017 apteka. All rights reserved.
//

import UIKit
import Spring

class ItemViewController: UIViewController, UISearchBarDelegate, openStackDelegate, SearchSettingsDelegate   {

    var parentNavigationController : UINavigationController?

    @IBOutlet var price: UILabel!
    @IBOutlet var pricePred: UILabel!
    @IBOutlet var prodStack: TovarStackView!
    @IBOutlet var formStack: TovarStackView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var priceView: UIView!
    @IBOutlet var bottomStack: UIStackView!

    //MARK:VAR
    
    @IBOutlet var active: UILabel! {
        didSet{
            if active != nil && self.bottomStack != nil{
                self.bottomStack.isHidden =  active.text == "" ? true : false
            }}
    }
    
    @IBAction func dppClick(_ sender: Any) {
        
    }
    
    var apteka : AptekaAddress! {
        didSet{
        }
    }
    
    var activeItem : Tovar?{
        didSet{
            if activeItem == nil{
                
            }
            else{
                self.setOneTovar(self.activeItem!)
            }
        }
    }
    
    var itemID : String? {
        didSet{
            self.searchCard(kod: self.itemID!, self.manId)
        }
    }
    var manId: String? {
        didSet{
            if self.itemID != nil {
                self.searchCard(kod: self.itemID!, self.manId)
            }
        }
    }
    var tovar : Tovar!


    //MARK:VAR Active

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        setStack()
        gestureSetting()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.activeItem = tovar
    
    }
    
    func setStack(){
        self.prodStack.createBtnsVariants(producers: [])
        self.prodStack.delegate = self
        
        self.formStack.createBtnsVariants(producers: [])
        self.formStack.delegate = self
        self.setTovs()
    }
    
    func setTovs(){
        
        let forms = self.tovar?.forms.compactMap{$0.title}.setValue ?? []
        let fuct = self.tovar?.manufacturers.compactMap{$0.title}.setValue ?? []

        self.formStack.createBtnsVariants(producers: forms)
        self.prodStack.createBtnsVariants(producers:fuct)
    }
    
    func setOneTovar(_ tov: Tovar){
        DispatchQueue.main.async {
            self.setImg(tov.imgKod)
            self.formStack.variatnSet(tov.form)
            if let titl = tov.manufacturer?.title{
                self.prodStack.variatnSet(titl)
            }
            else{
                self.prodStack.variatnSet(tov.manufacturers.first?.title ?? "")
            }
            self.active.text = tov.mnn
            self.setPrice(tov)
            self.bottomStack.isHidden = false
        }
    }
    
    
    
    func gestureSetting(){
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(ItemViewController.panGestureAction(_:)))
        panGestureRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(panGestureRecognizer)
    }

    
    func setPrice(_ tov: Tovar){
        
        if tov.price?.min == 0.0 && tov.price?.max == 0.0 {
            self.price.text = "Нет цены"
        }
        else{
            self.price.text = String(format:"%.2f₽ - %.2f₽", tov.price!.min, tov.price!.max)
        }
        if tov.priceOrder?.min == 0.0 && tov.priceOrder?.max == 0.0  {
            self.pricePred.text = "Нет цены"
        }
        else{
            self.pricePred.text = String(format:"%.2f₽ - %.2f₽", tov.priceOrder!.min, tov.priceOrder!.max)
        }
    }
    
    func setImg(_ kod:String){
        NetHelper.sharedInstance.imgLoad(kod) { (img) in
            if let image = img{
                self.imageView.image = image
            }
        }
    }
    
    func handleApteka(notification:NSNotification){
        if let ap = notification.object as? AptekaAddress{
            self.apteka = ap
        }
    }

    func settingsClick(_ on: Bool) {
        print("\(on)")
    }
    
    
    func prodClicked(titel:String, tag:Int){

        var res : [FormFaktor] = []
        
        if tag == 11 {
            res = self.tovar?.forms.filter{$0.title == titel} ?? []
        }
        else{
            res = self.tovar?.manufacturers.filter{$0.title == titel} ?? []
        }
        guard let f = res.first else {
            return
        }
        if tag == 11{
         self.itemID = f.kod
        }
        else{
            self.manId = f.kod
        }
        
    }
    
    
    func searchCard(kod:String, _ man:String?){
        self.view.showLoading()
        NetHelper.sharedInstance.tovarItem(kod, kodManuf: man) { tovar in
            self.view.hideLoading()
            
            if let tovar = tovar {
                self.activeItem = tovar
            }
        }
    }

    func open(with: Bool, and: TovarStackView) {
        print("\(with)")
        
    }
    

    var originalPosition: CGPoint?
    var currentPositionTouched: CGPoint?
    @objc func panGestureAction(_ panGesture: UIPanGestureRecognizer) {
        let translation = panGesture.translation(in: view)
        
        if panGesture.state == .began {
            originalPosition = view.center
            currentPositionTouched = panGesture.location(in: view)
        } else if panGesture.state == .changed {
            view.frame.origin = CGPoint(x: 0,y: translation.y)
        } else if panGesture.state == .ended {
            let velocity = panGesture.velocity(in: view)
            
            if  abs(velocity.y) >= abs(500){
                UIView.animate(withDuration: 0.2
                    , animations: {
                        self.view.frame.origin = CGPoint(x: self.view.frame.origin.x,y: self.view.frame.size.height)
                }, completion: { (isCompleted) in
                    if isCompleted {
                        self.dismiss(animated: false, completion: nil)
                    }
                })
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.center = self.originalPosition!
                })
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    deinit{
        print("deinit")
    }
}
