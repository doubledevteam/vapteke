//
//  ResultTableControlellr.swift
//  vapteke
//
//  Created by Ryan9Gray on 16.09.17.
//  Copyright © 2017 apteka. All rights reserved.
//

import UIKit
protocol HandleSearchResult : class {
    func searchResultName(_ name:String)
}
class SearchResultsController : UITableViewController, UISearchBarDelegate, UISearchControllerDelegate {
    var originalData : [String] = []
    var filteredData : [String] = []
    var word = ""
    weak var handleSearchDelegate:HandleSearchResult? = nil

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
       // self.extendedLayoutIncludesOpaqueBars = true
        

    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"Cell", for: indexPath)
        cell.textLabel!.text = self.filteredData[indexPath.row]
        return cell
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // to limit network activity, reload half a second after last key press.
        if searchText.isEmpty {

        }
    }
    
    @objc func reload(term:String){
        print("\(self.word)")
        if self.word == ""{
            return
        }
        NetHelper.sharedInstance.searchHint(self.word) { tovars in

            if let tov = tovars{
            self.filteredData = tov
            self.tableView.reloadData()
            }
        }
    }
//    func willPresentSearchController(_ searchController: UISearchController) {
//        self.navigationController?.navigationBar.isTranslucent = true
//    }
//
//    func willDismissSearchController(_ searchController: UISearchController) {
//        self.navigationController?.navigationBar.isTranslucent = false
//    }
}

extension SearchResultsController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = filteredData[indexPath.row]
        handleSearchDelegate?.searchResultName(selectedItem)
        dismiss(animated: true, completion: nil)
    }
}
extension SearchResultsController : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let sb = searchController.searchBar
        let target = sb.text!
        self.word  = target
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload), object: nil)
        self.perform(#selector(self.reload), with: nil, afterDelay: 0.5)

    }
}

