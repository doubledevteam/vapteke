//
//  City.swift
//  vapteke
//
//  Created by Ryan9Gray on 11.09.17.
//  Copyright © 2017 apteka. All rights reserved.
//

import Foundation

final public class City  {
    
    let cityID: String

    let phone: String
    let name: String

//    let longitude: Double?
//    let latitude: Double?
    
    
    init(cityID:String, phone:String, name:String) {
        self.cityID = cityID
        self.name = name
        self.phone = phone
//        self.longitude = long
//        self.latitude = lat
        //super.init()

    }
}
