//
//  MainNavigation.swift
//  Asko
//
//  Created by Ryan9Gray on 30.03.17.
//  Copyright © 2017 DoubleDevTeam. All rights reserved.
//

import UIKit

class MainNavigation: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if LocalStore.getCity() != nil{
            let view = storyboard!.instantiateViewController(withIdentifier: "MasterSearchID") as! MasterSearchViewController
            
            self.setViewControllers([view], animated: true)
        }
        else{
            let view = storyboard!.instantiateViewController(withIdentifier: "CityChoceID") as! CityChoseTableViewController
            
            self.setViewControllers([view], animated: true)
        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
