//
//  CityChoseTableViewController.swift
//  vapteke
//
//  Created by Ryan9Gray on 18.09.17.
//  Copyright © 2017 apteka. All rights reserved.
//

import UIKit
import Spring
import CoreLocation


class CityChoseTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, LocationServiceDelegate {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var okBtn: DesignableButton!

    @IBAction func okClick(_ sender: Any) {
        
        self.goToMain()
    }
    
    func tracingLocation(_ currentLocation: CLLocation) {
        
    }

    var cities : [City] = []{
        didSet{
            tableView.reloadData()
        }
    }
    
    var myCity : City! {
        didSet{
            LocalStore.setCity(self.myCity.name, idNum: self.myCity.cityID)
            self.okBtn.isHidden = false
        }
    }
    
    func tracingCity(_ city: String){
        
        let c = self.cities.filter{$0.name==city}
        
        if c.count > 0 {
            let v = self.cities.index{$0 === c[0]}
            let row = IndexPath.init(row: v!, section: 0)
            tableView.selectRow(at: row, animated: true, scrollPosition: .middle)
            self.title = "Это ваш город?"
        }
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        getCity()
    }

    func getCity(){
        self.view.showLoading()
        NetHelper.sharedInstance.getCitys { (cit) in
            if let city = cit{
                self.cities = city
                LocationService.sharedInstance.delegate = self
                LocationService.sharedInstance.startUpdatingLocation()
            }
            self.view.hideLoading()
        }
    }


    // MARK: - Table view data source

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.myCity = self.cities[indexPath.row]
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellCity", for: indexPath) as! CityTableViewCell
        
        cell.valueLabel.text = self.cities[indexPath.row].name.uppercased()
        // Настройка ячейки
        
        //cell.backgroundColor = UIColor.clear
        
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.cities.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        return 1
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
