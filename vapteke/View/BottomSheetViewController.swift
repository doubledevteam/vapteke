//
//  BottomSheetViewController.swift
//  vapteke
//
//  Created by Ryan9Gray on 29.10.2017.
//  Copyright © 2017 apteka. All rights reserved.
//

import UIKit
import Spring


class BottomSheetViewController: UIViewController {

    // holdView can be UIImageView instead
    @IBOutlet weak var closeBut: UIButton!

    @IBOutlet var infoView: UIView!
    @IBOutlet var apName: UILabel!
    @IBOutlet var apTime: UILabel!
    @IBOutlet var apPhone: UILabel!
    @IBOutlet var apAddress: UILabel!
    @IBOutlet var connectView: DesignableButton!
    @IBOutlet var nameView: UIView!
    @IBOutlet var addressView: UIView!
    
    var isOpen = false
    
    //private let pageHeight:CGFloat = 120
    
    var apteka : AptekaAddress! {
        didSet{
            setApteka(aptek:self.apteka)
        }
    }
    
    var fullView: CGFloat {
        return UIScreen.main.bounds.height - (self.infoView.frame.height+120)
    }
    
    var partialView: CGFloat = UIScreen.main.bounds.height - 120

    @IBAction func addAppClick(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "APTEKASEARCH"), object:self.apteka)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(BottomSheetViewController.panGesture))
        view.addGestureRecognizer(gesture)
        
        
        roundViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareBackgroundView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
            let frame = self?.view.frame
            let yComponent = self?.partialView
            self?.view.frame = CGRect(x: 0, y: yComponent!, width: frame!.width, height: frame!.height)
        })
    }
    
    func setApteka(aptek:AptekaAddress){
        apName.text = aptek.title
        apTime.text = aptek.timing
        apAddress.text = aptek.locationName
        apPhone.text = aptek.telephone
        //apInfoView.isHidden = false
        self.show(true)
    }

    
    func show(_ on : Bool){
        UIView.animate(withDuration: 0.3, animations: {
            let frame = self.view.frame
            if  on {
                self.view.frame = CGRect(x: 0, y: self.fullView, width: frame.width, height: frame.height)
            } else {
                self.view.frame = CGRect(x: 0, y: self.partialView, width: frame.width, height: frame.height)
            }
            self.closeBut.setImage(on ?  #imageLiteral(resourceName: "greenArrDown"):#imageLiteral(resourceName: "greenArrUp") , for: UIControlState())
            
        }, completion: nil)
    }
    
    func switchOpen(){
        self.show(isOpen)
        isOpen = !isOpen
    }
    
    @IBAction func close(_ sender: AnyObject) {
        self.switchOpen()
    }
    
    @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {
        
        let translation = recognizer.translation(in: self.view)
        let velocity = recognizer.velocity(in: self.view)
        let y = self.view.frame.minY
        if ( y + translation.y >= fullView) && (y + translation.y <= partialView ) {
            self.view.frame = CGRect(x: 0, y: y + translation.y, width: view.frame.width, height: view.frame.height)
            recognizer.setTranslation(CGPoint.zero, in: self.view)
        }
        
        if recognizer.state == .ended {
            var duration =  velocity.y < 0 ? Double((y - fullView) / -velocity.y) : Double((partialView - y) / velocity.y )
            
            duration = duration > 1.3 ? 1 : duration
            
            UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: {
                if  velocity.y >= 0 {
                    self.view.frame = CGRect(x: 0, y: self.partialView, width: self.view.frame.width, height: self.view.frame.height)
                    self.closeBut.setImage(#imageLiteral(resourceName: "greenArrUp"), for: .normal)
                    self.isOpen = false
                } else {
                    self.view.frame = CGRect(x: 0, y: self.fullView, width: self.view.frame.width, height: self.view.frame.height)
                    self.closeBut.setImage(#imageLiteral(resourceName: "greenArrDown"), for: .normal)
                    self.isOpen = true
                }
                
                }, completion: nil)
        }
    }
    
    func roundViews() {
        view.layer.cornerRadius = 5
        view.clipsToBounds = true
    }
    
    func prepareBackgroundView(){
        let blurEffect = UIBlurEffect.init(style: .light)
        let visualEffect = UIVisualEffectView.init(effect: blurEffect)
        let bluredView = UIVisualEffectView.init(effect: blurEffect)
        bluredView.contentView.addSubview(visualEffect)
        
        visualEffect.frame = UIScreen.main.bounds
        bluredView.frame = UIScreen.main.bounds
        
        view.insertSubview(bluredView, at: 0)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
