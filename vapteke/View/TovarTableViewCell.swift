//
//  TovarTableViewCell.swift
//  vapteke
//
//  Created by Ryan9Gray on 10.12.2017.
//  Copyright © 2017 apteka. All rights reserved.
//

import UIKit
import Foundation

protocol photoClickDelegate : class {
    func openImageTag(_ imageTag:String)
    func setName(tovar:Cure) -> NSAttributedString

}
extension photoClickDelegate where Self : UIViewController {

    func setName(tovar:Cure) -> NSAttributedString{
        
        let formattedString = NSMutableAttributedString()
        formattedString
            .bold(tovar.title.uppercased())
            .normal(", \(tovar.form.uppercased())")
        
        return formattedString
    }
    
}

class TovarTableViewCell: UITableViewCell {

    @IBOutlet var priceLbl: UILabel!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var photoBtn: UIButtonX!
    @IBOutlet var moreBtn: UIButtonX!
    
    weak var photoDelegate : photoClickDelegate?
    var tovar : Cure!
    
    @IBAction func moreClick(_ sender: Any) {
        
    }
    @IBAction func photoClick(_ sender: Any) {
        self.photoDelegate?.openImageTag(self.tovar.imgKod)
    }
    
    func setTovar(tovar:Cure){
        self.tovar = tovar
        nameLbl.attributedText = self.photoDelegate?.setName(tovar: tovar)
        priceLbl.text = String(format:"%.2f₽", tovar.priceMin)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
