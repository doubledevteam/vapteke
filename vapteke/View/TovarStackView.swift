//
//  TovarStackView.swift
//  vapteke
//
//  Created by Ryan9Gray on 24.09.17.
//  Copyright © 2017 apteka. All rights reserved.
//
import Foundation
import UIKit
protocol openStackDelegate {
    func open(with: Bool, and:TovarStackView)
    func prodClicked(titel:String, tag:Int)
    
}

class TovarStackView: UIStackView {

    var delegate: openStackDelegate?
    @IBOutlet var title: UILabel!
    @IBOutlet var subTitle: UILabel!
    @IBOutlet var heightView: NSLayoutConstraint!
    @IBOutlet var producerView: UIScrollView!
    @IBOutlet var arr: UIImageView!
    var isOpen = false

    //MARK:VAR
    
    @IBAction func producerClick(_ sender: Any) {
        close()
        self.delegate?.open(with: isOpen, and: self)
    }

    func close(){
        self.producerView.isHidden = isOpen
        self.arr.image = UIImage.init(named: isOpen ? "arrDown" : "arrUp")
        isOpen = !isOpen
    }
    
    func createBtnsVariants(producers:[String]){
        for v in self.producerView.subviews{
            v.removeFromSuperview()
        }
        self.free()

        if producers.count == 1{
            self.variatnSet(producers[0])
        }
        
        self.heightView.constant = producers.count < 4 ? CGFloat(producers.count*60 + (10*producers.count) + 10) : CGFloat(220)
        
        for (index, variant) in producers.enumerated(){

            let btn = UIButton.init(frame: CGRect(x: 21, y: 60*index + (10*index), width: Int(self.frame.width-21), height: 60))
            btn.addTarget(self, action: #selector(self.variantClick(_:)), for: UIControlEvents.touchUpInside)
            btn.backgroundColor = UIColor.clear
            btn.setTitle(variant, for: UIControlState())
            btn.titleLabel?.font = UIFont(name: "HelveticaNeue", size: 15)
            btn.setTitleColor(UIColor.black, for: UIControlState())
            btn.contentHorizontalAlignment = .left
            btn.layoutIfNeeded()
            btn.tag = index
            self.producerView.contentSize = CGSize(width: self.producerView.frame.size.width, height: CGFloat(producers.count*60 + (10*producers.count) + 10))
            self.producerView.addSubview(btn)
        }
        self.layoutIfNeeded()
    }
    @objc func variantClick(_ sender: UIButton){
        if self.producerView.subviews.count == 1{
            return
        }
        
        let title = sender.titleLabel?.text
        self.subTitle.text = title!.uppercased()
        self.close()
        delegate?.prodClicked(titel: title!, tag: (sender.superview?.tag)!)
    }
    
    func variatnSet(_ variant:String){
        self.subTitle.text = variant.uppercased()
        self.subTitle.textColor = .black
    }
    func free(){
        self.subTitle.text = "НЕ ВЫБРАНО"
        self.subTitle.textColor = UIColor.darkGray//UIColor(red:0.47, green:0.47, blue:0.47, alpha:1.00)
        
    }
}
