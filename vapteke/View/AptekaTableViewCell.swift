//
//  AptekaTableViewCell.swift
//  vapteke
//
//  Created by Ryan9Gray on 24.09.17.
//  Copyright © 2017 apteka. All rights reserved.
//

import UIKit

class AptekaTableViewCell: UITableViewCell {

    @IBOutlet var time: UILabel!
    @IBOutlet var name: UILabel!
    @IBOutlet var phone: UILabel!
    @IBOutlet var address: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setUpCell(apteka:AptekaAddress){
        self.name.text = apteka.title
        self.time.text = apteka.timing
        self.phone.text = apteka.telephone
        self.address.text = apteka.locationName
    }

}
