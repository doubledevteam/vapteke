import UIKit

protocol SearchSettingsDelegate {
    func settingsClick(_ on:Bool)
}

class CustomSearchController: UISearchController, UISearchBarDelegate, UITextFieldDelegate {
    
    var settingsDelegate : SearchSettingsDelegate!
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }

    // MARK: Initialization
    
    init(searchResultsController: UIViewController!, searchBarFrame: CGRect, searchBarFont: UIFont, searchBarTextColor: UIColor, searchBarTintColor: UIColor) {
        super.init(searchResultsController: searchResultsController)
        //self.searchResultsController = searchResultsController
        configureSearchBar(searchBarFrame, font: searchBarFont, textColor: searchBarTextColor, bgColor: searchBarTintColor)
    }
    
    init(searchResultsController: UIViewController!, searchBarFrame: CGRect) {
        super.init(searchResultsController: searchResultsController)

        configureSearchBar(searchBarFrame, font: UIFont(name: "Futura", size: 18.0)!, textColor: UIColor.black, bgColor: UIColor.customGrey())
    }
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    
    
    // MARK: Custom functions
    
    func configureSearchBar(_ frame: CGRect, font: UIFont, textColor: UIColor, bgColor: UIColor) {
        
        self.searchBar.frame = frame
        self.searchBar.barTintColor = bgColor
        self.searchBar.tintColor = textColor
        self.searchBar.showsBookmarkButton = false
        self.searchBar.showsCancelButton = false
        self.searchBar.showsSearchResultsButton = false
        
       // self.searchBar.setShowsCancelButton(false, animated: false)
        
        self.searchBar.searchBarStyle = UISearchBarStyle.prominent
        self.searchBar.isTranslucent = false
        self.searchBar.keyboardAppearance = .dark
        self.searchBar.showsCancelButton = false
        self.hidesNavigationBarDuringPresentation = false
        self.dimsBackgroundDuringPresentation = true
        self.definesPresentationContext = true
        self.isActive = false
        
        if let index = indexOfSearchFieldInSubviews() {
            // Access the search field
            let searchField: UITextField = (self.searchBar.subviews[0] ).subviews[index] as! UITextField
            // Set its frame.    public init(x: Double, y: Double, width: Double, height: Double)
            searchField.frame = CGRect(x:5.0,y: 5.0,width: frame.size.width - 10.0,height: frame.size.height - 10.0)
            // Set the font and text color of the search field.
            searchField.font = font
            searchField.textColor = textColor
            searchField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0)
            // Set the background color of the search field.
            searchField.backgroundColor = bgColor
            //searchField.delegate = self
        }
        
        self.searchBar.setValue("", forKey:"_cancelButtonText")
        let cross = UIImage(named: "cross")?.imageResize(sizeChange:CGSize(width: 24, height: 24))
        self.searchBar.setImage(cross, for: UISearchBarIcon.clear, state: .normal)
        self.searchBar.setImage(UIImage(named: "search"), for: UISearchBarIcon.search, state: .normal)

//        let imageButton = UIButton(type: .custom)
//        imageButton.frame = CGRect(x: self.searchBar.frame.width - 40, y: (self.searchBar.frame.height - 28)/2.0, width: 28, height: 28)
//        //imageButton.setImage(UIImage(named: "settingsOff")).wi
//        imageButton.setImage(UIImage(named:"settingsOff")?.withRenderingMode(.alwaysOriginal), for: .normal)
//        imageButton.setImage(UIImage(named:"settingsOn")?.withRenderingMode(.alwaysOriginal), for: .selected)

        //imageButton.setImage(UIImage(named: "settingsOn"), for:UIControlState.selected).withRenderingMode(.alwaysOriginal), for: .selected)
        //imageButton.addTarget(self, action: #selector(self.clickSettings(_ :)), for: UIControlEvents.touchUpInside)
        //self.searchBar.addSubview(imageButton)

    }
    
    func clickSettings(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        self.settingsDelegate.settingsClick(!sender.isSelected)
    }
    
    func indexOfSearchFieldInSubviews() -> Int! {
        var index: Int!
        let searchBarView = self.searchBar.subviews[0]
        
        for i in 0 ..< searchBarView.subviews.count {
            if searchBarView.subviews[i].isKind(of:UITextField.self) {
                index = i
                break
            }
        }
        return index
    }
    
    func indexOfCancelInSubviews() -> Int! {
        var index: Int!
        let searchBarView = self.searchBar.subviews[0]
        for i in 0 ..< searchBarView.subviews.count {
       
            if searchBarView.subviews[i].isKind(of: NSClassFromString("UINavigationButton")!) {
                let cancelButtonAttributes: NSDictionary = [NSAttributedStringKey.foregroundColor: UIColor.white]
                UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes as? [NSAttributedStringKey : AnyObject], for: UIControlState.normal)
                index = i
                break
            }
        }
        return index
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
