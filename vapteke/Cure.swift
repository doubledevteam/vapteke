//
//  Cure.swift
//  vapteke
//
//  Created by Ryan9Gray on 11.09.17.
//  Copyright © 2017 apteka. All rights reserved.
//

import Foundation

final class Cure  {

    var title: String = ""
    var imgKod : String = ""
    var tovKod : String = ""
    var mnn : String = ""
    var form : String = ""
    
    var priceMin : Double = 0.00

    var isSynonym = false
    var manufacturer : FormFaktor? = nil
    
}

final class Tovar  {
    
    var title: String = ""
    var tovKod : String = ""
    var imgKod : String = ""
    var mnn : String = ""
    var form : String = ""

    var manufacturer : FormFaktor? = nil
    var manufacturers : [FormFaktor] = []
    var forms : [FormFaktor] = []
    
    var price: Price? = nil
    var priceOrder: Price? = nil
}

final class Price {
    let max:Double!
    let min:Double!
    init(max:Double, min:Double) {
        self.max = max
        self.min = min
    }
}
final class FormFaktor  {
    let title: String!
    let kod: String!
    init(title:String, kod:String) {
        self.title = title
        self.kod = kod
    }
}

