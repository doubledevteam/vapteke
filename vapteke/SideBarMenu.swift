//
//  SideBarMenu.swift
//  Asko
//
//  Created by Ryan9Gray on 04.07.16.
//  Copyright © 2016 DoubleDevTeam. All rights reserved.
//

import UIKit


class SideBarMenu: UITableViewController {

    
    @IBOutlet var cityName: UILabel!
    var functional:Bool = false
    
    
    @IBAction func cityClick(_ sender: Any) {
        
        
    }
    
    func callMaim(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var idC = ""
        if functional{
            idC = ""
        }
        else{
            idC = ""
        }
        let vc = storyboard.instantiateViewController(withIdentifier: idC)
        let rvc:SWRevealViewController = self.revealViewController() as SWRevealViewController
        rvc.pushFrontViewController(vc, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.clearsSelectionOnViewWillAppear = true

        //self.tableView.allowsMultipleSelection = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }


    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if let city = LocalStore.getCity(){
            self.cityName.text = "\(city["name"]!)".uppercased()
        }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "continSeg" {
            
        }
    }
    
    
    // MARK: <TableViewDataSource>
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        

//        if let selectedIndex = tableView.indexPathForSelectedRow where selectedIndex == indexPath {
//            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
//        }

        
        if indexPath.row == 1 {
        }

//        let selectedCell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
//        selectedCell.contentView.backgroundColor = UIColor(red:0.18, green:0.31, blue:0.60, alpha:1.0)

    }

    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cellToDeSelect:UITableViewCell = tableView.cellForRow(at: indexPath)!
        cellToDeSelect.contentView.backgroundColor = UIColor.clear
        
        
        
    }

    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        

        return 64.0
    }

    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        tableView.cellForRow(at: indexPath)?.contentView.backgroundColor = UIColor.clear
//        let imageName = "menuCellLine"
//        let image = UIImage(named: imageName)
//        let imageView = UIImageView(image: image!)
//        imageView.frame = CGRect(x: 0, y: 0, width: 5, height: 64)
//        view.addSubview(imageView)
//        cell.contentView.addSubview(imageView)

    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        footerView.backgroundColor = UIColor.white
       
        return footerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section != 0 {
            return 1
        }
        return 0    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
